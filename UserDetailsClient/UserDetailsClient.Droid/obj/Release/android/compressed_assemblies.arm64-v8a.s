	.arch	armv8-a
	.file	"compressed_assemblies.arm64-v8a.arm64-v8a.s"
	.include	"compressed_assemblies.arm64-v8a-data.inc"

	.section	.data.compressed_assembly_descriptors,"aw",@progbits
	.type	.L.compressed_assembly_descriptors, @object
	.p2align	3
.L.compressed_assembly_descriptors:
	/* 0: FormsViewGroup.dll */
	/* uncompressed_file_size */
	.word	14336
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_0

	/* 1: Java.Interop.dll */
	/* uncompressed_file_size */
	.word	164352
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_1

	/* 2: Microsoft.CSharp.dll */
	/* uncompressed_file_size */
	.word	300032
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_2

	/* 3: Microsoft.Identity.Client.dll */
	/* uncompressed_file_size */
	.word	1306112
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_3

	/* 4: Mono.Android.dll */
	/* uncompressed_file_size */
	.word	2125824
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_4

	/* 5: Mono.Security.dll */
	/* uncompressed_file_size */
	.word	121856
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_5

	/* 6: Newtonsoft.Json.dll */
	/* uncompressed_file_size */
	.word	682496
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_6

	/* 7: Plugin.CurrentActivity.dll */
	/* uncompressed_file_size */
	.word	9216
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_7

	/* 8: System.Core.dll */
	/* uncompressed_file_size */
	.word	1073664
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_8

	/* 9: System.Data.dll */
	/* uncompressed_file_size */
	.word	747520
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_9

	/* 10: System.Drawing.Common.dll */
	/* uncompressed_file_size */
	.word	26112
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_10

	/* 11: System.Net.Http.dll */
	/* uncompressed_file_size */
	.word	222208
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_11

	/* 12: System.Numerics.dll */
	/* uncompressed_file_size */
	.word	38912
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_12

	/* 13: System.Runtime.Serialization.dll */
	/* uncompressed_file_size */
	.word	419328
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_13

	/* 14: System.ServiceModel.Internals.dll */
	/* uncompressed_file_size */
	.word	55808
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_14

	/* 15: System.Text.Encodings.Web.dll */
	/* uncompressed_file_size */
	.word	52736
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_15

	/* 16: System.Xml.Linq.dll */
	/* uncompressed_file_size */
	.word	74240
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_16

	/* 17: System.Xml.dll */
	/* uncompressed_file_size */
	.word	1397760
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_17

	/* 18: System.dll */
	/* uncompressed_file_size */
	.word	894976
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_18

	/* 19: UserDetailsClient.Core.dll */
	/* uncompressed_file_size */
	.word	32768
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_19

	/* 20: UserDetailsClient.Droid.dll */
	/* uncompressed_file_size */
	.word	243200
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_20

	/* 21: Xamarin.AndroidX.Activity.dll */
	/* uncompressed_file_size */
	.word	18432
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_21

	/* 22: Xamarin.AndroidX.Annotation.dll */
	/* uncompressed_file_size */
	.word	6144
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_22

	/* 23: Xamarin.AndroidX.AppCompat.AppCompatResources.dll */
	/* uncompressed_file_size */
	.word	17920
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_23

	/* 24: Xamarin.AndroidX.AppCompat.dll */
	/* uncompressed_file_size */
	.word	461824
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_24

	/* 25: Xamarin.AndroidX.Arch.Core.Common.dll */
	/* uncompressed_file_size */
	.word	6656
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_25

	/* 26: Xamarin.AndroidX.Arch.Core.Runtime.dll */
	/* uncompressed_file_size */
	.word	6144
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_26

	/* 27: Xamarin.AndroidX.AsyncLayoutInflater.dll */
	/* uncompressed_file_size */
	.word	6144
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_27

	/* 28: Xamarin.AndroidX.Browser.dll */
	/* uncompressed_file_size */
	.word	58368
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_28

	/* 29: Xamarin.AndroidX.CardView.dll */
	/* uncompressed_file_size */
	.word	16896
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_29

	/* 30: Xamarin.AndroidX.Collection.dll */
	/* uncompressed_file_size */
	.word	6656
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_30

	/* 31: Xamarin.AndroidX.CoordinatorLayout.dll */
	/* uncompressed_file_size */
	.word	70656
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_31

	/* 32: Xamarin.AndroidX.Core.dll */
	/* uncompressed_file_size */
	.word	538624
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_32

	/* 33: Xamarin.AndroidX.CursorAdapter.dll */
	/* uncompressed_file_size */
	.word	6656
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_33

	/* 34: Xamarin.AndroidX.CustomView.dll */
	/* uncompressed_file_size */
	.word	6656
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_34

	/* 35: Xamarin.AndroidX.DocumentFile.dll */
	/* uncompressed_file_size */
	.word	6144
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_35

	/* 36: Xamarin.AndroidX.DrawerLayout.dll */
	/* uncompressed_file_size */
	.word	40448
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_36

	/* 37: Xamarin.AndroidX.Fragment.dll */
	/* uncompressed_file_size */
	.word	154112
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_37

	/* 38: Xamarin.AndroidX.Interpolator.dll */
	/* uncompressed_file_size */
	.word	6656
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_38

	/* 39: Xamarin.AndroidX.Legacy.Support.Core.UI.dll */
	/* uncompressed_file_size */
	.word	15360
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_39

	/* 40: Xamarin.AndroidX.Legacy.Support.Core.Utils.dll */
	/* uncompressed_file_size */
	.word	6144
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_40

	/* 41: Xamarin.AndroidX.Lifecycle.Common.dll */
	/* uncompressed_file_size */
	.word	16384
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_41

	/* 42: Xamarin.AndroidX.Lifecycle.LiveData.Core.dll */
	/* uncompressed_file_size */
	.word	16384
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_42

	/* 43: Xamarin.AndroidX.Lifecycle.LiveData.dll */
	/* uncompressed_file_size */
	.word	6144
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_43

	/* 44: Xamarin.AndroidX.Lifecycle.Runtime.dll */
	/* uncompressed_file_size */
	.word	6144
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_44

	/* 45: Xamarin.AndroidX.Lifecycle.ViewModel.dll */
	/* uncompressed_file_size */
	.word	17920
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_45

	/* 46: Xamarin.AndroidX.Lifecycle.ViewModelSavedState.dll */
	/* uncompressed_file_size */
	.word	5632
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_46

	/* 47: Xamarin.AndroidX.Loader.dll */
	/* uncompressed_file_size */
	.word	35840
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_47

	/* 48: Xamarin.AndroidX.LocalBroadcastManager.dll */
	/* uncompressed_file_size */
	.word	6144
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_48

	/* 49: Xamarin.AndroidX.Media.dll */
	/* uncompressed_file_size */
	.word	11264
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_49

	/* 50: Xamarin.AndroidX.MediaRouter.dll */
	/* uncompressed_file_size */
	.word	16896
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_50

	/* 51: Xamarin.AndroidX.Palette.dll */
	/* uncompressed_file_size */
	.word	6656
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_51

	/* 52: Xamarin.AndroidX.Print.dll */
	/* uncompressed_file_size */
	.word	5632
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_52

	/* 53: Xamarin.AndroidX.RecyclerView.dll */
	/* uncompressed_file_size */
	.word	403968
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_53

	/* 54: Xamarin.AndroidX.SavedState.dll */
	/* uncompressed_file_size */
	.word	13312
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_54

	/* 55: Xamarin.AndroidX.SlidingPaneLayout.dll */
	/* uncompressed_file_size */
	.word	6656
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_55

	/* 56: Xamarin.AndroidX.SwipeRefreshLayout.dll */
	/* uncompressed_file_size */
	.word	26624
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_56

	/* 57: Xamarin.AndroidX.Transition.dll */
	/* uncompressed_file_size */
	.word	9728
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_57

	/* 58: Xamarin.AndroidX.VectorDrawable.Animated.dll */
	/* uncompressed_file_size */
	.word	7680
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_58

	/* 59: Xamarin.AndroidX.VectorDrawable.dll */
	/* uncompressed_file_size */
	.word	6656
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_59

	/* 60: Xamarin.AndroidX.VersionedParcelable.dll */
	/* uncompressed_file_size */
	.word	6656
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_60

	/* 61: Xamarin.AndroidX.ViewPager.dll */
	/* uncompressed_file_size */
	.word	54272
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_61

	/* 62: Xamarin.Essentials.dll */
	/* uncompressed_file_size */
	.word	31744
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_62

	/* 63: Xamarin.Forms.Core.dll */
	/* uncompressed_file_size */
	.word	1190400
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_63

	/* 64: Xamarin.Forms.Platform.Android.dll */
	/* uncompressed_file_size */
	.word	799232
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_64

	/* 65: Xamarin.Forms.Platform.dll */
	/* uncompressed_file_size */
	.word	132608
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_65

	/* 66: Xamarin.Forms.Xaml.dll */
	/* uncompressed_file_size */
	.word	102400
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_66

	/* 67: Xamarin.Google.Android.Material.dll */
	/* uncompressed_file_size */
	.word	179200
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_67

	/* 68: Xamarin.Google.Guava.ListenableFuture.dll */
	/* uncompressed_file_size */
	.word	18072
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_68

	/* 69: mscorlib.dll */
	/* uncompressed_file_size */
	.word	2200576
	/* loaded */
	.byte	0
	/* data */
	.zero	3
	.xword	compressed_assembly_data_69

	.size	.L.compressed_assembly_descriptors, 1120
	.section	.data.compressed_assemblies,"aw",@progbits
	.type	compressed_assemblies, @object
	.p2align	3
	.global	compressed_assemblies
compressed_assemblies:
	/* count */
	.word	70
	/* descriptors */
	.zero	4
	.xword	.L.compressed_assembly_descriptors
	.size	compressed_assemblies, 16
