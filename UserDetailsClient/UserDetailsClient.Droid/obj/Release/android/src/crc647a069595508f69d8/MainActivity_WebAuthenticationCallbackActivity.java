package crc647a069595508f69d8;


public class MainActivity_WebAuthenticationCallbackActivity
	extends crc64a0e0a82d0db9a07d.WebAuthenticatorCallbackActivity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("UserDetailsClient.Droid.MainActivity+WebAuthenticationCallbackActivity, UserDetailsClient.Droid", MainActivity_WebAuthenticationCallbackActivity.class, __md_methods);
	}


	public MainActivity_WebAuthenticationCallbackActivity ()
	{
		super ();
		if (getClass () == MainActivity_WebAuthenticationCallbackActivity.class)
			mono.android.TypeManager.Activate ("UserDetailsClient.Droid.MainActivity+WebAuthenticationCallbackActivity, UserDetailsClient.Droid", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
