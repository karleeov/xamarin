﻿using System;
using System.Net.Http;
using Microsoft.Identity.Client;
using UserDetailsClient.Core.Features.LogOn;
using Xamarin.Forms;
using Xamarin.Essentials;


namespace UserDetailsClient.Core
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            const string CALLBACK_SCHEME = "myapp";

        }


        async void OnSign(object sender, EventArgs e)
        {

            if (btnSignIn.Text == "Sign in")
            {
                var authResult = await WebAuthenticator.AuthenticateAsync(
          new Uri("https://mybupadevb2c.b2clogin.com/mybupadevb2c.onmicrosoft.com/B2C_1A_B2C_1_signin/oauth2/v2.0/logout?post_logout_redirect_uri=https%3A%2F%2Fjwt.ms%2F"),
          new Uri("myapp://app"));
                var userContext = authResult?.AccessToken;
                Console.WriteLine(userContext);
            }
        }
            
       
        async void OnSignInSignOut(object sender, EventArgs e)
        {
            try
            {
                if (btnSignInSignOut.Text == "Sign in")
                {
                    var authResult = await WebAuthenticator.AuthenticateAsync(
              new Uri("https://mybupadevb2c.b2clogin.com/mybupadevb2c.onmicrosoft.com/oauth2/v2.0/authorize?p=B2C_1A_B2C_1_Bupa&client_id=64a33cb0-3424-4e3d-883d-cd256d3b5e32&nonce=defaultNonce&redirect_uri=https%3A%2F%2Fjwt.ms%2F&scope=openid&response_type=id_token"),
              new Uri("myapp://app"));

                    var accessToken = authResult?.AccessToken;
                    Console.WriteLine(accessToken);

                }
                else
                {
                    var userContext = await B2CAuthenticationService.Instance.SignOutAsync();
                    UpdateSignInState(userContext);
                    UpdateUserInfo(userContext);
                }
            }
            catch (Exception ex)
            {
                // Checking the exception message 
                // should ONLY be done for B2C
                // reset and not any other error.
                if (ex.Message.Contains("AADB2C90118"))
                    OnPasswordReset();
                // Alert if any exception excluding user canceling sign-in dialog
                else if (((ex as MsalException)?.ErrorCode != "authentication_canceled"))
                    await DisplayAlert($"Exception:", ex.ToString(), "Dismiss");
            }
        }




        async void OnCallApi(object sender, EventArgs e)
        {
            try
            {
                lblApi.Text = $"Calling API {App.ApiEndpoint}";
                var userContext = await B2CAuthenticationService.Instance.SignInAsync();
                var token = userContext.AccessToken;
                Console.WriteLine(token);


                // Get data from API
                HttpClient client = new HttpClient();
                HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Get, App.ApiEndpoint);
                message.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                HttpResponseMessage response = await client.SendAsync(message);
                string responseString = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    lblApi.Text = $"Response from API {App.ApiEndpoint} | {responseString}";
                }
                else
                 {
                    lblApi.Text = $"Error calling API {App.ApiEndpoint} | {responseString}";
                }
            }
            catch (MsalUiRequiredException ex)
            {
                await DisplayAlert($"Session has expired, please sign out and back in.", ex.ToString(), "Dismiss");
            }
            catch (Exception ex)
            {
                await DisplayAlert($"Exception:", ex.ToString(), "Dismiss");
            }
        }

        async void OnEditProfile(object sender, EventArgs e)
        {
            try
            {
                var userContext = await B2CAuthenticationService.Instance.EditProfileAsync();
                UpdateSignInState(userContext);
                UpdateUserInfo(userContext);
            }
            catch (Exception ex)
            {
                // Alert if any exception excluding user canceling sign-in dialog
                if (((ex as MsalException)?.ErrorCode != "authentication_canceled"))
                    await DisplayAlert($"Exception:", ex.ToString(), "Dismiss");
            }
        }
        async void OnResetPassword(object sender, EventArgs e)
        {
            try
            {
                var userContext = await B2CAuthenticationService.Instance.ResetPasswordAsync();
                UpdateSignInState(userContext);
                UpdateUserInfo(userContext);
            }
            catch (Exception ex)
            {
                // Alert if any exception excluding user canceling sign-in dialog
                if (((ex as MsalException)?.ErrorCode != "authentication_canceled"))
                    await DisplayAlert($"Exception:", ex.ToString(), "Dismiss");
            }
        }
        async void OnPasswordReset()
        {
            try
            {
                var userContext = await B2CAuthenticationService.Instance.ResetPasswordAsync();
                UpdateSignInState(userContext);
                UpdateUserInfo(userContext);
            }
            catch (Exception ex)
            {
                // Alert if any exception excluding user canceling sign-in dialog
                if (((ex as MsalException)?.ErrorCode != "authentication_canceled"))
                    await DisplayAlert($"Exception:", ex.ToString(), "Dismiss");
            }
        }

        void UpdateSignInState(UserContext userContext)
        {
            var isSignedIn = userContext.IsLoggedOn;
            btnSignInSignOut.Text = isSignedIn ? "Sign out" : "Sign in";
            btnEditProfile.IsVisible = isSignedIn;
            btnCallApi.IsVisible = isSignedIn;
            slUser.IsVisible = isSignedIn;
            lblApi.Text = "";
        }
        public void UpdateUserInfo(UserContext userContext)
        {
            lblName.Text = userContext.Name;
            lblJob.Text = userContext.JobTitle;
            lblCity.Text = userContext.City;
        }
        public void test()
        {
            var authResult = WebAuthenticator.AuthenticateAsync(
            new Uri("https://mybupauatb2c.b2clogin.com/mybupauatb2c.onmicrosoft.com/oauth2/v2.0/authorize?p=B2C_1_king&client_id=177f5121-24f2-416d-b659-f12b21e801ab&nonce=defaultNonce&redirect_uri=https%3A%2F%2Fjwt.ms%2F&scope=openid&response_type=id_token&prompt=login"),
            new Uri("myapp://"));

            //var accessToken = authResult?.AccessToken;
        }

    }


}