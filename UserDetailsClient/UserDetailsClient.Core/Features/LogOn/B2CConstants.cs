﻿namespace UserDetailsClient.Core.Features.LogOn
{
    public static class B2CConstants
    {
        // Azure AD B2C Coordinates
        public static string Tenant = "mybupauatb2c.onmicrosoft.com";
        public static string AzureADB2CHostname = "mybupauatb2c.b2clogin.com";
        public static string ClientID = "177f5121-24f2-416d-b659-f12b21e801ab";
        public static string PolicySignUpSignIn = "B2C_1_signin";
        public static string PolicyEditProfile = "b2c_1_edit_profile";
        public static string PolicyResetPassword = "b2c_1_reset";

        public static string[] Scopes = { "https://mybupauatb2c.b2clogin.com/mybupauatb2c.onmicrosoft.com/oauth2/v2.0/authorize?p=B2C_1_signin&client_id=177f5121-24f2-416d-b659-f12b21e801ab&nonce=defaultNonce&redirect_uri=https%3A%2F%2Fjwt.ms%2F&scope=openid&response_type=id_token" };

        public static string AuthorityBase = $"https://{AzureADB2CHostname}/tfp/{Tenant}/";
        public static string AuthoritySignInSignUp = $"{AuthorityBase}{PolicySignUpSignIn}";
        public static string AuthorityEditProfile = $"{AuthorityBase}{PolicyEditProfile}";
        public static string AuthorityPasswordReset = $"{AuthorityBase}{PolicyResetPassword}";
        public static string IOSKeyChainGroup = "com.microsoft.adalcache";
    }
}